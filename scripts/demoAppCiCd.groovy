import javaposse.jobdsl.dsl.DslFactory
import javaposse.jobdsl.dsl.Job
import javaposse.jobdsl.dsl.*


def jobName = "DemoApp_Ci_Cd"
def desc = "DemoApp Workshop CI CD Flow"
def pipelinScript = "./scripts/DemoApp_Ci_Cd_Pipeline"

pipelineJob(jobName) {  
    description(desc)
    concurrentBuild(false)
    logRotator(8,8)
	disabled(false)
    
    triggers {
            scm('* * * * *')
    }

    definition {
        cps {
            script(readFileFromWorkspace(pipelinScript).stripIndent())
       	    sandbox()
        }
    }
}
